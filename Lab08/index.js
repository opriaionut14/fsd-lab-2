exports.helloWorld = (req, res) => {
    res.send('Hello, World');
  };

const Datastore = require('@google-cloud/datastore');
const datastore = new Datastore({
	projectId: 'smooth-hub-310916',
	keyFilename: 'datastore-credential.json'
});
/*
const kindName = 'user-log';

exports.savelog = (req, res) => {
	let uid = req.query.uid || req.body.uid || 0;
	let log = req.query.log || req.body.log || '';

	datastore
		.save({
			key: datastore.key(kindName),
			data: {
				log: log,
				uid: datastore.int(uid),
				time_create: datastore.int(Math.floor(new Date().getTime()/1000))
			}
		})
		.catch(err => {
		    console.error('ERROR:', err);
		    res.status(500).send(err);
		    return;
		});

	res.status(200).send(log);
};*/

const kindName = "user-log";
exports.savelog = (req, res) => {
  let uid = req.query.uid || req.body.uid || 0;
  let log = req.query.log || req.body.log || "";
  datastore
    .save({
      key: datastore.key(kindName),
      data: {
        log: log,
        uid: datastore.int(uid),
        createdAt: new Date(),
      },
    })
    .catch((err) => {
      console.error("ERROR:", err);
      res.status(500).send(err);
      return;
    });
  res.status(200).send(log);
};