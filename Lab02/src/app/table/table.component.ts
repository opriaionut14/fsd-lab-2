import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  img_name: string;
  img_size: string;
  recognition_result: string;
  download_link: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {img_name: 'Frozen yogurt',       img_size: "1280x720", recognition_result: 'Yup, it\'s definitely yogurt', download_link: 'Click me'},
  {img_name: 'Ice cream sandwich',  img_size: "1280x720", recognition_result: 'It may be a plane', download_link: 'Click me'},
  {img_name: 'Eclair',              img_size: "1280x720", recognition_result: 'Something very sweet', download_link: 'Click me'},
  {img_name: 'Cupcake',             img_size: "1280x720", recognition_result: 'Pankakes?', download_link: 'Click me'},
  {img_name: 'Gingerbread',         img_size: "1280x720", recognition_result: 'Gingerbread', download_link: 'Click me'},
  {img_name: 'Jelly bean',          img_size: "1280x720", recognition_result: 'Slime!!!', download_link: 'Click me'},
  {img_name: 'Lolipop',             img_size: "1280x720", recognition_result: 'Suck on this', download_link: 'Click me'},
  {img_name: 'Honeycomb',           img_size: "1280x720", recognition_result: 'Maybe honeycomb', download_link: 'Click me'},
  {img_name: 'Donut',               img_size: "1280x720", recognition_result: 'Every cop\'s favorite snack', download_link: 'Click me'},
  {img_name: 'KitKat',              img_size: "1280x720", recognition_result: 'Kitkat', download_link: 'Click me'},
];

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  constructor() { }

  displayedColumns: string[] = ['img_name', 'img_size', 'recognition_result', 'download_link'];
  dataSource = ELEMENT_DATA;

  ngOnInit(): void {
  }

  goToLink(){
    window.open("https://www.youtube.com/watch?v=dQw4w9WgXcQ", "_blank");
}
}
