import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HeaderComponent} from "../app/header/header.component";
import { HomePageComponent } from './home-page/home-page.component';
import { TableComponent } from './table/table.component';
import { UploadImgComponent } from './upload-img/upload-img.component';

const routes: Routes = [
  {path:"header", component:HeaderComponent},
  { path: "table", component:TableComponent },
  { path: "upload-img", component:UploadImgComponent },
  { path: "", component:HomePageComponent },
  { path: "**", redirectTo: "" }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
